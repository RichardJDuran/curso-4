//jasmine spec/models/bicicletas_test.spec.js

var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');

describe ('Testing Bicicletas', function()  {

    beforeEach(function(done){
        var mongoDB = "mongodb://localhost/testdb";
        mongoose.disconnect();
        mongoose.connect(mongoDB, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true  });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Error en la conexión'));
        db.once('open', function(){
            console.log('Conexion con testdb');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function( err, success ){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance',() => {
        it('Creamos una instancia de Bibicleta', () => {
            var bici = Bicicleta.createInstance(1,'Roja','Urbana',[-33.4,-70.6]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('Roja');
            expect(bici.modelo).toBe('Urbana');
            expect(bici.ubicacion[0]).toEqual(-33.4);
            expect(bici.ubicacion[1]).toEqual(-70.6);
        });
    });

    describe('Bicicleta.allBicis',() => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicis(function(err,bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add',() => {
        it('Creamos una instancia de Bibicleta', (done) => {
            var aBici = Bicicleta({ code: 1, color : 'Roja', modelo : 'Urbana'});
            Bicicleta.add(aBici,function(err,newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err,bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);  
                    done();
                })
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            const aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, ( err, newBici) => {
                if (err) console.log(err);
                var aBici2 = new Bicicleta({code:2, color:"roja", modelo:"urbana"});
                Bicicleta.findByCode(1,function( err, targetBici ) { 
                    expect(targetBici.code).toEqual(aBici.code);
                    expect(targetBici.color).toEqual(aBici.color);
                    expect(targetBici.modelo).toEqual(aBici.modelo);
                    done();
                });
            });
        });
    });

});
