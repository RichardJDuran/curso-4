var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
var bcrypt = require('bcrypt');
var crypto = require('crypto');
var Token = require('./token');
const mailer = require('../mailer/mailer');

const saltRound = 10;

var Schema = mongoose.Schema;

const validateEmail = function (email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingresar un email válido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpire: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRound);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
};


usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({ usuario:this._id, bicicleta:biciId, desde:desde, hasta:hasta });
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;

    console.log('token => ' + token);
    console.log('email destination => ' + email_destination);

    token.save(function( err ){
        if( err ) {
            return console.log(err.message);
        }
        
        const mailOptions = {
            from: 'no-reply@red-bicicletas.cl',
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: 'Hola, \n\n'+' para verificar su cuenta haga click en este enlace: \n' + process.env.HOST+'\/token/confirmation\/'+token.token + '\n' 
        } 

        mailer.sendMail(mailOptions, function(err) {
            if (err) { return console.log(err.message); }
            console.log('Un email de verificacion ha sido enviado a ' + email_destination + '.');
        });
        
    });
};

usuarioSchema.methods.resetPassword = function(cb) {
    const token = new Token({_userId: this._id,token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;

    token.save(function( err ){
        if( err ) {
            return console.log(err.message);
        }
        
        const mailOptions = {
            from: 'no-reply@redbicicleta.com',
            to: email_destination,
            subject: 'Reestablecer password',
            text: 'Hola, \n\n'+' para reestablecer su password haga click en este enlace: \n' + 'http://localhost:5000'+'\/resetPassword\/'+token.token + '\n' 
        }

        mailer.sendMail(mailOptions, ( err )=> {
            if(err) { return console.log(err.message); }
            console.log('a reset password email has been sent to ' + email_destination);
        });
    });
    
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
            $or: [{
                    googleId: condition.id,
                },
                {
                    email: condition.emails[0].value,
                },
            ],
        },
        (err, result) => {
            if (result) {
                callback(err, result);
            } else {
                console.log('------------CONDITION-------------');
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex'); //condition._json.etag;
                console.log('------------VALUES----------');
                console.log(values);
                self.create(values, (err, result) => {
                    if (err) {
                        console.log(err);
                    }
                    return callback(err, result);
                });
            }
        }
    );
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[{'facebookId': condition.id},{'email': condition.emails[0].value}]
    },(err,result) => {
        if (result) {
            callback(err, result);
        } else {
            console.log('---- Condition ----');
            let values = {};
            values.facebookId= condition.id;
            values.email= condition.emails[0].value;
            values.nombre= condition.displayName||'Sin nombre';
            values.verificado= true;
            values.password= crypto.randomBytes(16).toString('hex');
            console.log('---- Values ----');
            console.log(values);
            self.create(values,(err, result) => {
                if (err) {console.log(err);}
                return callback(err,result);
            });
        }
    });
};


module.exports = mongoose.model('Usuario', usuarioSchema);