# Red Bicicletas

Este proyecto fue generado con [Node JS](https://nodejs.org/es/) version 12.16.3.

## Instalar Módulos de Node

Ejecute `npm install` dentro de la carpeta del proyecto **red-bicicletas**, para descargar los paquetes o modulos de node requeridos para poder ejecutar la aplicación. 

## Servidor de Desarrollo

Ejecute `npm run devstart` para iniciar el servidor web. Navegue a `http://localhost:3000/`

## Tests con Jasmine

Ejecute `npm test` para ejecutar las pruebas de modelos de Bicicleta y Usuario y de API de Bicicleta de la carpeta spec.

## Ruta de Lista de Bicicletas (CRUD)

- http://localhost:3000/bicicletas

## Endpoint de la API de Bicicletas

1. API GET - Listar Bicicletas   => http://localhost:3000/api/bicicletas
2. API POST - Crear Bicicleta    => http://localhost:3000/api/bicicletas/create
3. API PUT - Modificar Bicicleta => http://localhost:3000/api/bicicletas/update
4. API DELETE - Borrar Bicicleta => http://localhost:3000/api/bicicletas/delete

## Endpoint de la API de Usuarios

1. API GET - Listar Usuarios     => http://localhost:3000/api/usuarios
2. API POST - Crear Usuario      => http://localhost:3000/api/usuarios/create
3. API POST - Crear Reserva      => http://localhost:3000/api/usuarios/reservar
